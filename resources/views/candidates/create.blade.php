@extends('layouts.app')

@section('content')  
        <h1 class="text-center font-weight-bold p-3 mb-2 bg-secondary">Create candidate</h1>
            <form method = "post" action = "{{action('CandidatesController@store')}}">
                @csrf
                <div>
                     <label for = "name" class="col-xs-2 col-sm-4 col-md-2 control-label">Candidate name</label>
                     <input type = "text" name = "name">
                </div>
                <div>
                     <label for = "email" class="col-xs-2 col-sm-4 col-md-2 control-label">Candidate email</label>
                     <input type = "text" name = "email">
                </div>
                <div>
                     <input type = "submit" class="text-center font-weight-bold btn btn-outline-dark mb-2 bg-secondary" name = "Create candidate">
                </div>
            </form> 
@endsection
