@extends('layouts.app')

@section('content')    
    <h1 class="text-center font-weight-bold p-3 mb-2 bg-secondary">Edit candidate</h1>
        <form method = "post" action = "{{action('CandidatesController@update',$candidate->id)}}">
        @csrf
        @METHOD('PATCH')
        <div>
            <label for = "name" class="col-xs-2 col-sm-4 col-md-2 control-label">Candiadte name</label>
            <input type = "text" name = "name" value = {{$candidate->name}}>
        </div>     
        <div>
            <label for = "email" class="col-xs-2 col-sm-4 col-md-2 control-label">Candiadte email</label>
            <input type = "text" name = "email" value = {{$candidate->email}}>
        </div> 
        <div>
            <input type = "submit" name = "submit" class="text-center font-weight-bold btn btn-outline-dark mb-2 bg-secondary" value = "Update candidate">
        </div>                       
        </form>    
@endsection
