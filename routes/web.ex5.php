<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#ex5
Route::get('/users/{name?}/{email}', function ($name=null,$email) {
    if (!isset($name)){
        $name = 'missing name';
    }
    return view('users',compact('name','email'));
});

##ex5
Route::get('/users/{name?}/{email}', function ($name=null,$email) {
    if (!isset($name)){
        $name = 'missing name';
    }
    return view('users',compact('name','email'));
});